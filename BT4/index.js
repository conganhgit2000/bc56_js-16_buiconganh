function inKetQua() {  
  
  for (var i = 0; i < 10; i++) {
    var div = document.createElement("div");
    div.textContent = "Div " + (i + 1);

    if (i % 2 === 0) {
      div.classList.add("red");
    } else {
      div.classList.add("blue");
    }

    document.getElementById("container").appendChild(div);
  }
  
}