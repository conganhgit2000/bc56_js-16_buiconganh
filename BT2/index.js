function tinhTong() {
    var numberX = document.getElementById("number-x").value * 1;
    var numberN = document.getElementById("number-n").value * 1;

    var sum = 0;

    for (var i = 1; i <= numberN; i++) {
        sum += Math.pow(numberX, i);
    }

    document.getElementById('result').innerHTML = "Tổng S(" + numberN +") = " + sum;
}