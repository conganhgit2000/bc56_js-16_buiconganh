function tinhGiaiThua() {
    var numberN = document.getElementById("number-n").value * 1;

    var giaiThua = 1;

    for (var i = 1; i <= numberN; i++) {
        giaiThua *= i;
    }

    document.getElementById("result").innerHTML = "Giai thừa của " + numberN + " là: " + giaiThua;
}